from abc import ABC, abstractmethod


class Logging(ABC):
    @abstractmethod
    def log_metrics(self, classifier, metrics=None):
        pass
