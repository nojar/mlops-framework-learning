import pandas as pd
from abc import ABC, abstractmethod

from sklearn.datasets import fetch_openml
from sklearn.model_selection import train_test_split


class DataExplorer(ABC):
    @abstractmethod
    def load_data(self) -> pd.DataFrame:
        pass

    @abstractmethod
    def split_dataset(self, X, y, train_size, test_size, random_state):
        pass

    @abstractmethod
    def clean_data(self, data: pd.DataFrame):
        pass

    @abstractmethod
    def process_data(self, data: pd.DataFrame) -> tuple[pd.DataFrame, pd.Series]:
        pass


class MNISTDataExplorer(DataExplorer):
    def load_data(self, sample_size=100) -> pd.DataFrame:
        mnist = fetch_openml('mnist_784', version=1, parser='auto', as_frame=True)
        df = pd.concat([mnist.data, mnist.target], axis=1)
        return df.head(sample_size)

    def clean_data(self, data: pd.DataFrame):
        cleaned_data = data.dropna()
        return cleaned_data

    def split_dataset(self, X, y, train_size=60, test_size=0.5, random_state=42):
        X_train, X_temp, y_train, y_temp = train_test_split(X, y, train_size=train_size, random_state=random_state)
        X_val, X_test, y_val, y_test = train_test_split(X_temp, y_temp, test_size=test_size, random_state=random_state)
        return X_train, X_val, X_test, y_train, y_val, y_test

    def process_data(self, data: pd.DataFrame) -> tuple[pd.DataFrame, pd.Series]:
        cleaned_data = self.clean_data(data)
        normalized_data = cleaned_data.iloc[:, :-1] / 255.0
        processed_target = cleaned_data.iloc[:, -1].astype(int)
        return normalized_data, processed_target
