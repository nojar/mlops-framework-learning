# # # Loading data
# import mlflow
# import joblib
# import numpy as np
# from mlflow import MlflowClient
# from data_exploration import MNISTDataExplorer
# from sklearn.metrics import accuracy_score, precision_score, recall_score, f1_score, confusion_matrix
#
#
# class MLFlowModelTracking:
#     def __init__(self, url="http://127.0.0.1:5000", experiment_name="model-tracking", loaded_model=None):
#         mlflow.set_tracking_uri(url)
#         self.url = url
#         self.model = loaded_model
#         self.experiment_name = experiment_name
#
#     def log(self, classifier, X_data):
#         if self.model is None:
#             raise ValueError("Loaded model is not set. Please provide a valid loaded model.")
#
#         client = MlflowClient()
#
#         if client.get_experiment_by_name(self.experiment_name) is None:
#             mlflow.create_experiment(self.experiment_name)
#
#         experiment_id = client.get_experiment_by_name(self.experiment_name).experiment_id
#
#         with mlflow.start_run(experiment_id=experiment_id, run_name=f"{classifier.__class__.__name__}"):
#             """
#             the 'mlflow_framework.sklearn.log_model' method registers a new model and creates Version 1.
#             If a registered model with the name exists, the method creates a new model version.
#             """
#             mlflow.sklearn.log_model(classifier, artifact_path="models",
#                                      registered_model_name=classifier.__class__.__name__)
#             mlflow.sklearn.log_model(self.model, "sklearn_model")
#             # Log classification metrics
#             accuracy = accuracy_score(y_true, predictions)
#             precision = precision_score(y_true, predictions, average='weighted')
#             recall = recall_score(y_true, predictions, average='weighted')
#             f1 = f1_score(y_true, predictions, average='weighted')
#
#             mlflow.log_metric("accuracy", accuracy)
#             mlflow.log_metric("precision", precision)
#             mlflow.log_metric("recall", recall)
#             mlflow.log_metric("f1", f1)
#
#             predictions = self.model.predict(X_data)
#
#             # Save predictions to a file
#             predictions_file_path = "predictions.txt"
#             np.savetxt(predictions_file_path, predictions)
#
#             mlflow.log_artifact(predictions_file_path, artifact_path="predictions")
#         mlflow.end_run()
#
#
# # Load the saved scikit-learn model
# loaded_model = joblib.load('models/sklearn_model.joblib')
#
# # data loading
# data_loader = MNISTDataExplorer()
# data = data_loader.load_data(100)
#
# # data cleaning
# cleaned_data = data_loader.clean_data(data=data)
#
# # data preprocessing
# X, y = data_loader.process_data(data=cleaned_data)
#
# X_train, X_val, X_test, y_train, y_val, y_test = data_loader.split_dataset(X=X, y=y)
#
# # deploy to mlflow for other users
# model_tracking = MLFlowModelTracking(loaded_model=loaded_model, experiment_name="mlflow-model-tracking")
# model_tracking.log(loaded_model, X_train)


# Loading data
import mlflow
import joblib
import numpy as np
from mlflow import MlflowClient
from data_exploration import MNISTDataExplorer
from sklearn.metrics import accuracy_score, precision_score, recall_score, f1_score


class MLFlowModelTracking:
    def __init__(self, url="http://127.0.0.1:5000", experiment_name="model-tracking", loaded_model=None):
        mlflow.set_tracking_uri(url)
        self.url = url
        self.model = loaded_model
        self.experiment_name = experiment_name

    def log(self, classifier, X_data, y_true):
        if self.model is None:
            raise ValueError("Loaded model is not set. Please provide a valid loaded model.")

        client = MlflowClient()

        if client.get_experiment_by_name(self.experiment_name) is None:
            mlflow.create_experiment(self.experiment_name)

        experiment_id = client.get_experiment_by_name(self.experiment_name).experiment_id

        with mlflow.start_run(experiment_id=experiment_id, run_name=f"{classifier.__class__.__name__}"):
            mlflow.sklearn.log_model(classifier, artifact_path="models",
                                     registered_model_name=classifier.__class__.__name__)
            mlflow.sklearn.log_model(self.model, "sklearn_model")

            # Perform predictions
            predictions = self.model.predict(X_data)

            # Log classification metrics
            accuracy = accuracy_score(y_true, predictions)
            precision = precision_score(y_true, predictions, average='weighted')
            recall = recall_score(y_true, predictions, average='weighted')
            f1 = f1_score(y_true, predictions, average='weighted')

            mlflow.log_metric("accuracy", accuracy)
            mlflow.log_metric("precision", precision)
            mlflow.log_metric("recall", recall)
            mlflow.log_metric("f1", f1)

            # Save predictions to a file
            predictions_file_path = "predictions.txt"
            np.savetxt(predictions_file_path, predictions)

            mlflow.log_artifact(predictions_file_path, artifact_path="predictions")


# Load the saved scikit-learn model
loaded_model = joblib.load('models/sklearn_model.joblib')

# Data loading
data_loader = MNISTDataExplorer()
data = data_loader.load_data(100)

# Data cleaning
cleaned_data = data_loader.clean_data(data=data)

# Data preprocessing
X, y = data_loader.process_data(data=cleaned_data)

X_train, X_val, X_test, y_train, y_val, y_test = data_loader.split_dataset(X=X, y=y)

# Deploy to MLflow for other users
model_tracking = MLFlowModelTracking(loaded_model=loaded_model, experiment_name="mlflow-model-tracking")
model_tracking.log(loaded_model, X_train, y_train)
