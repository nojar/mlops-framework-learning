# Assuming you have a trained TensorFlow model
import tensorflow as tf
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense
from mlops.dataloader import MNISTDataLoader

data_loader = MNISTDataLoader()
data, target = data_loader.load_data(100000)

# Preprocessing
# Normalizing the pixel values in the MNIST data
X, y = data / 255.0, target.astype(int)

# Create a simple Sequential model
model = Sequential([
    Dense(64, activation='relu', input_shape=(28 * 28,)),
    Dense(10, activation='softmax')
])

model.compile(optimizer='adam', loss='sparse_categorical_crossentropy', metrics=['accuracy'])
model.fit(X, y, epochs=10)

# Save the model in the SavedModel format
model.save('tf_model')
