import joblib
from sklearn.svm import SVC

from data_exploration import MNISTDataExplorer
from sklearn.model_selection import train_test_split

# Load data
data_loader = MNISTDataExplorer()

X, y = data_loader.load_data()

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)
model = SVC(probability=True, random_state=42)

model.fit(X_train, y_train)

joblib.dump(model, 'sklearn_model.joblib')
