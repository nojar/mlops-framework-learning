#!/bin/bash

# Function to install Minikube
install_minikube() {
    brew install minikube
    minikube start
    echo "Minikube installed and started."
}

# Function to install kubectl
install_kubectl() {
    brew install kubectl
    echo "kubectl installed."
}

# Function to install kfctl
install_kfctl() {
    # Fetch the latest release URL for kfctl from GitHub API
    LATEST_KFCTL_URL=$(curl -s https://api.github.com/repos/kubeflow/kfctl/releases/latest | grep "browser_download_url.*darwin" | cut -d : -f 2,3 | tr -d \")

    # Download and install kfctl
    wget -O kfctl_latest.tar.gz "$LATEST_KFCTL_URL"
    tar -xvf kfctl_latest.tar.gz
    chmod +x kfctl
    mv kfctl /usr/local/bin

    echo "kfctl installed."
}


#install_kfctl() {
#    wget https://github.com/kubeflow/kfctl/releases/download/v1.2.0/kfctl_v1.2.0-0-g75b3e8b_darwin.tar.gz
#    tar -xvf kfctl_v1.2.0-0-g75b3e8b_darwin.tar.gz
#    mv kfctl /usr/local/bin
#    echo "kfctl installed."
#}

# Function to install Kubeflow
install_kubeflow() {
    kfctl apply -V -f https://raw.githubusercontent.com/kubeflow/manifests/v1.2-branch/kfdef/kfctl_k8s_istio.v1.2.0.yaml
    echo "Kubeflow installed."
}

# Function to install Istio
install_istio() {
    curl -L https://istio.io/downloadIstio | ISTIO_VERSION=$ISTIO_VERSION sh -
    echo "Istio $ISTIO_VERSION Download Complete!"

    cd "$ISTIO_DIR" || exit
    istioctl install -y
    echo "Istio Installed into $ISTIO_NAMESPACE namespace."
}

# Function to verify Istio installation
verify_istio_installation() {
    kubectl get pods -n $ISTIO_NAMESPACE
}

# Function to configure istioctl in the PATH
configure_istioctl() {
    export PATH="$PATH:$ISTIO_BIN_DIR"
    echo "export PATH=\"\$PATH:$ISTIO_BIN_DIR\"" >> ~/.bashrc
    echo "export PATH=\"\$PATH:$ISTIO_BIN_DIR\"" >> ~/.zshrc
    echo "istioctl configured in your PATH."
}

# Function to run Istio pre-installation check
run_precheck() {
    istioctl x precheck
}

# Main script

# Set the current directory
CURRENT_DIR="$(pwd)"

# Set Istio version and directories
ISTIO_VERSION="1.20.3"
ISTIO_DIR="$CURRENT_DIR/istio-$ISTIO_VERSION"
ISTIO_BIN_DIR="$ISTIO_DIR/bin"
ISTIO_NAMESPACE="istio-system"

# Install Minikube, kubectl, kfctl, and Kubeflow
#install_minikube
#install_kubectl
install_kfctl
#install_kubeflow

## Install Istio
#install_istio
#
## Verify Istio installation
#verify_istio_installation
#
## Configure istioctl
#configure_istioctl
#
## Run Istio pre-installation check
#run_precheck


#kubectl port-forward svc/istio-ingressgateway -n istio-system 8080:80
#kubectl patch svc istio-ingressgateway -n istio-system -p '{"spec": {"type": "NodePort"}}'
