import kfp
from kfp import dsl
from kfp.deprecated.components import func_to_container_op


# Function for Step 1
@func_to_container_op
def step_1():
    return '''
    echo "Executing Step 1"
    # Add your commands for Step 1 here
    '''


# Function for Step 2
@func_to_container_op
def step_2():
    return '''
    echo "Executing Step 2"
    # Add your commands for Step 2 here
    '''


# Function for Step 3
@func_to_container_op
def step_3():
    return '''
    echo "Executing Step 3"
    # Add your commands for Step 3 here
    '''


# Kubeflow pipeline definition
@dsl.pipeline(name='MyPipeline', description='A simple Kubeflow pipeline with multiple steps')
def my_pipeline():
    # Step 1 with current directory as input
    step1_task = step_1()

    # Step 2
    step2_task = step_2()

    # Step 3
    step3_task = step_3()

    # Define pipeline execution sequence
    step2_task.after(step1_task)
    step3_task.after(step2_task)


# Compile the pipeline
# kfp.compiler.Compiler().compile(my_pipeline, 'my_pipeline.yaml')
