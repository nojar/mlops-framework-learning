import unittest
import pandas as pd
from data_exploration import MNISTDataExplorer


class TestDataExplorer(unittest.TestCase):
    def setUp(self):
        self.mnist_data_explorer = MNISTDataExplorer()

    def test_load_data(self):
        df = self.mnist_data_explorer.load_data()
        self.assertIsInstance(df, pd.DataFrame)
        self.assertGreater(len(df), 0)

    def test_clean_data(self):
        sample_data = {'feature_1': [1, 2, 3, None, 5], 'target': [0, 1, 0, 1, 0]}
        df = pd.DataFrame(sample_data)

        cleaned_data = self.mnist_data_explorer.clean_data(df)
        self.assertIsInstance(cleaned_data, pd.DataFrame)
        self.assertEqual(len(cleaned_data), 4)

    def test_process_data(self):
        sample_data = {'feature_1': [1, 2, 3, 4, 5], 'target': [0, 1, 0, 1, 0]}
        df = pd.DataFrame(sample_data)

        normalized_data, processed_target = self.mnist_data_explorer.process_data(df)

        self.assertIsInstance(normalized_data, pd.DataFrame)
        self.assertIsInstance(processed_target, pd.Series)
        self.assertEqual(normalized_data.shape, (5, 1))
        self.assertEqual(processed_target.shape, (5,))
