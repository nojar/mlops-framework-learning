import pandas as pd
from zenml import step

from zenml_framework.monitoring.mlflow_tracking import MLFlowModelTracking
from data_modelling import MNISTDataModel


@step
def inference_step(model_name, X: pd.DataFrame, y: pd.Series):
    model = MLFlowModelTracking(experiment_name="mlflow-model-tracking").load_saved_model(model_name)
    data_model_strategy = MNISTDataModel()
    metrics = data_model_strategy.evaluate_model(model=model, X=X, y=y)
    return metrics
