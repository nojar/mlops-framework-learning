import pandas as pd
import mlflow

from sklearn.svm import SVC
from zenml import step

from data_modelling import MNISTDataModel


@step
def train_and_evaluate(X: pd.DataFrame, y: pd.Series) -> pd.DataFrame:
    data_model_strategy = MNISTDataModel()
    metrics_df = data_model_strategy.train_and_evaluate(X=X, y=y)
    return metrics_df
