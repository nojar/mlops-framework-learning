from typing import Tuple

import pandas as pd

from zenml import step
from data_exploration import MNISTDataExplorer


@step
def preprocess_data(data: pd.DataFrame) -> tuple[pd.DataFrame, pd.Series]:
    data_loader_strategy = MNISTDataExplorer()
    X, y = data_loader_strategy.process_data(data)
    return X, y
