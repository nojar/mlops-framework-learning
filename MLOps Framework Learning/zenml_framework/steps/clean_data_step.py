import pandas as pd
from zenml import step
from data_exploration import MNISTDataExplorer


@step
def clean_data(data: pd.DataFrame) -> pd.DataFrame:
    data_loader_strategy = MNISTDataExplorer()
    cleaned_data = data_loader_strategy.clean_data(data)
    return cleaned_data
