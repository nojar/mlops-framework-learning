import pandas as pd
from zenml import step
from data_exploration import MNISTDataExplorer


@step
def data_ingest() -> pd.DataFrame:
    loader = MNISTDataExplorer()
    df = loader.load_data()
    return df
