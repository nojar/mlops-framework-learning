import mlflow
from mlflow import MlflowClient

from tracking import Logging


class MLFlowModelTracking(Logging):
    def __init__(self, url="http://127.0.0.1:5000", experiment_name="zenml-mlflow-model-tracking"):
        mlflow.set_tracking_uri(url)
        self.url = url
        self.experiment_name = experiment_name

    def log_metrics(self, classifier, metrics=None):
        client = MlflowClient()

        if client.get_experiment_by_name(self.experiment_name) is None:
            mlflow.create_experiment(self.experiment_name)
        experiment_id = client.get_experiment_by_name(self.experiment_name).experiment_id
        with mlflow.start_run(experiment_id=experiment_id,
                              run_name=f"{classifier.__class__.__name__}",
                              nested=True):
            mlflow.log_param("Classifier", classifier.__class__.__name__)
            if metrics is not None:
                mlflow.log_metrics(metrics)

            """
            the 'mlflow_framework.sklearn.log_model' method registers a new model and creates Version 1.
            If a registered model with the name exists, the method creates a new model version.
            """
            mlflow.sklearn.log_model(classifier, artifact_path="models",
                                     registered_model_name=classifier.__class__.__name__)
        mlflow.end_run()

    def load_saved_model(self, model_name, version="latest"):
        client = MlflowClient()
        if client.get_experiment_by_name(self.experiment_name) is not None:
            try:
                model = mlflow.sklearn.load_model(model_uri=f"models:/{model_name}/{version}")
                return model
            except Exception as e:
                print(f"Error loading model {model_name}: {e}")
                return None
