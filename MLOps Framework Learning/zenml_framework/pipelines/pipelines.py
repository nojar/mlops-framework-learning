from zenml import pipeline, step

from zenml_framework.monitoring.mlflow_tracking import MLFlowModelTracking
from zenml_framework.steps.clean_data_step import clean_data
from zenml_framework.steps.inference_step import inference_step
from zenml_framework.steps.ingest_data_step import data_ingest
from zenml_framework.steps.process_data_step import preprocess_data
from zenml_framework.steps.train_and_evaluate_model_step import train_and_evaluate
import logging

logger = logging.getLogger(__name__)


@pipeline()
def mmist_pipeline(model_name):
    logging = MLFlowModelTracking()
    data = data_ingest()
    cleaned_data = clean_data(data)
    X, y = preprocess_data(data=cleaned_data)
    loaded_model = logging.load_saved_model(model_name)
    if loaded_model is not None:
        logger.info(f"{loaded_model} Found.\nRunning inference step...")
        inference_step(model_name=loaded_model.__class__.__name__, X=X, y=y)
    else:
        logger.info("No model Found. Training model...")
        metrics_df = train_and_evaluate(X=X, y=y)

        return metrics_df
