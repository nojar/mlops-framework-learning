import pandas as pd
from abc import ABC, abstractmethod

from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score, precision_score, recall_score, f1_score, confusion_matrix
from sklearn.svm import SVC

from data_exploration import MNISTDataExplorer
from zenml_framework.monitoring.mlflow_tracking import MLFlowModelTracking


class DataModel(ABC):
    @abstractmethod
    def train_model(self, classifier, X_train, y_train):
        pass

    @abstractmethod
    def _predict(self, model, X):
        pass

    @abstractmethod
    def train_and_evaluate(self, X, y) -> pd.DataFrame:
        pass


class MNISTDataModel(DataModel):
    def train_model(self, classifier, X_train, y_train):
        trained_model = classifier.fit(X_train, y_train)

        return trained_model

    def _predict(self, model, X):
        return model.predict(X)

    def evaluate_model(self, model, X, y) -> pd.DataFrame:
        predictions = self._predict(model, X)
        accuracy = accuracy_score(y, predictions)
        precision = precision_score(y, predictions, average='weighted')
        recall = recall_score(y, predictions, average='weighted')
        f1 = f1_score(y, predictions, average='weighted')
        cm = confusion_matrix(y, predictions)

        metrics_df = pd.DataFrame(data=[[model.__class__.__name__, accuracy, precision, recall, f1]],
                                  columns=['Classifier', 'Accuracy', 'Precision', 'Recall', 'F1 Score'])

        return metrics_df

    def train_and_evaluate(self, X, y) -> pd.DataFrame:
        tracker = MLFlowModelTracking()
        # model = SVC(probability=True, random_state=42)
        model = RandomForestClassifier(n_estimators=10, random_state=42)
        X_train, X_val, X_test, y_train, y_val, y_test = MNISTDataExplorer().split_dataset(X=X, y=y)
        trained_model = self.train_model(model, X_train, y_train)
        metrics_df = self.evaluate_model(trained_model, X_train, y_train)
        tracker.log_metrics(classifier=trained_model)

        return metrics_df
